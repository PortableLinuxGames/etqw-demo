ETQW Dedicated Server and Client for GNU/Linux x86 - Demo Version
=================================================================

Important system requirements:
------------------------------

You need a low latency kernel for optimal performance (this applies to both client and server installations). Make sure your kernel
is configured with CONFIG_HZ_1000=y. You should also enable other low latency settings, such as the various preemption settings.

Installation:
-------------

You need a retail copy of the game to install the full client. The server package comes with all the data unless you download a -nomedia
or -update package, in which case you are expected to copy some files manually. See in the '-nomedia installers' section below for a list
of files and checksums.

To start the server, use:
etqw-dedicated
To start the client, use:
etqw
To start the client with renderer threading support, use:
etqw-rthread

Start the server like this:
./etqw-dedicated <parameters>
To execute a server.cfg config file:
./etqw-dedicated +exec server.cfg

Start the client with the etqw script

The start script makes sure the game binaries will find the dynamic libraries in the same directory.

Minimum system requirements:
----------------------------

- GNU/Linux x86 system
x64/amd64 systems supported through 32 bit emulation layer only
BSD systems through Linux ABI compatibility
- Pentium(r) 4 2.0 GHz or Athlon(tm) XP 2000+ processor
- 512MB RAM
- Kernel 2.6, configured for 1000Hz tick and other low latency settings

3D acceleration (not required for server):
   - SDL >= 1.2 [1] ( >= 1.2.10 recommended )
   - latest OpenGL drivers - correctly configured!
   - nVidia(r) GeForce 5700 (except LE and VE)
   - ATI(r) Radeon 9700

Audio backends for OSS and Alsa (+set s_driver alsa/oss - defaults to Alsa)

Multiplayer:
- Internet (TCP/IP) and LAN (TCP/IP) play supported
- Internet play requires broadband connection

Ports:
------

In order to host an Internet server, the following ports must be open on your firewall:

Incoming:
UDP: 27733 (use +set net_port on the command line to change this)
UDP: 3074

Outgoing:
TCP: 3074

-nomedia installers:
--------------------

If the installer you are using is a 'no media' installer (i.e. there is -nomedia in the installer
file name), then you are expected to copy a number of files from a Windows installation of the
game into the base/ folder. If you do not want / do not know how to do this, download the full
installer (which we already make available with the nomedia one)

Here is the current file list with MD5 checksums:

The gamecode paks (game*.pk4) are not listed here, all installers (including -update and -nomedia)
carry the correct gamecode paks.

For the server:
2c4d435997fed37266ff7b97aa233c3f  pak000.pk4
ef38c591f5611d38358b221962f39a1b  pak001.pk4
3589df65359be8bf8b571f28ff66639b  pak002.pk4

For the client:
2f7428ba0bc34265e2770eeb86f7dfb2  megatextures/valley_lit.mega
04c26a5404512eded9232677dfec8c86  pak000.pk4
c26f06044eeb74692a5aec362598a772  pak001.pk4
f71311acd5ca982dfdd9a6bc9c882d99  pak002.pk4
7c174823c677ceb28abea9fd962abdd1  zpak_english000.pk4

Changes:
--------

Fri Jan  4 12:37:31 CST 2008
- updated demo setup - merged all latest 1.4 work into demo code
- SDL joystick support
as for the Windows version there is no GUI to configure, you need to read:
http://community.enemyterritory.com/forums/showpost.php?p=55937
listController shows the joysticks that are bound
you can also set in_showJoy to see joystick buttons and axes printed to the console
tested with one xbox 360 controller
other joysticks and multiple joysticks should work fine, but is untested

Wed Dec 26 16:08:30 CST 2007
- support for render threading with a new binary: etqw-rthread.x86
this requires a modified libSDL however (1.2.12), which is provided
the patch to the source modifications is provided as well
in this binary, r_displayRefresh can be modified (still defaults to 0 however)

Mon Dec 17 10:48:44 CST 2007
- r_displayRefresh is not supported on Linux (SDL provides no support for this)
updated the cvar to avoid confusion
- cleaned up some input code hacks in SDL that are now covered by better i18n support in game core
- bind more keys through SDL (print/menu/pause/scrollock), distinguish left/right on shift/ctrl/alt
- support the lwin/rwin key binds (official gamecode may not allow binding them yet)

Mon Dec  3 12:56:00 CST 2007
- fix crash if mic failed to init when trying to read sound (Alsa backend)

Wed Nov 28 11:35:07 CST 2007
- support r_swapInterval cvar to control vsync (was not hooked up before)
only supported in SDL >= 1.2.10
the attribute reporting for it seems broken on most distributions
- add s_alsa_mic to use a different Alsa device for the mic input
- add sys_nohup cvar to ignore SIGHUP signals (if you ever need it)

Fri Nov 16 09:55:43 CST 2007
- new client build, r5

Thu Nov 15 18:31:11 CST 2007
- finished voice input (OSS and Alsa backends)
- fixed the dsp device from s_dsp -> s_device
- s_noMic to skip the mic init / voice input

Fri Nov  9 11:21:07 CST 2007
- by default, don't require Ctrl+Alt+~ for console toggle
- receive voice (can't send yet)
- r_useThreadedRenderer hardcoded to 0 - is not supported in the Linux build atm

Thu Nov  8 09:30:39 CST 2007
- fix showFPSBandings crash, com_showFPS needs to be enabled
- make SIGHUP shutdown more explicit, as it's not a crash
but rather a requested shutdown (controlling terminal exited)

Wed Nov  7 09:57:28 CST 2007
- set r_useFBODestinationBuffer to 0 when detecting an NVidia card
(required for 5700 series otherwise performance degrades too much)
- misc updates to fix potential gamecode crashes

Thu Nov  1 11:47:49 CDT 2007
- add zpaks to the setups

Wed Oct 31 11:17:43 CDT 2007
- server fix from SD, doing r3 server builds

Tue Oct 30 18:28:58 CDT 2007
- produce 1.2 r2 builds
- compatible with 1.2 servers
- hax OSS/Alsa backend to work for the new sound architecture - VOIP is not implemented yet
- fix a threading related crash on the client
(may affect server code though no specific crash identified,
doing a server update still)

Mon Oct 29 12:34:21 CDT 2007
- 1.2 server builds

Bug reports:
------------

See the FAQ for tips, known issues and bug reports instructions:
http://zerowing.idsoftware.com/linux/etqw/

enjoy!
TTimo

[1] - http://www.libsdl.org/
